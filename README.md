# Scala MegaTondeuse Project 

## Compile / Test and Run

Pour compiler le projet vous devez vous situez dans le repertoire de ce dernier.
Une fois dedans, vous devriez avoir un fichier *build.sbt*

**Compile**

Utilisez la commande suivante pour compiler

*sbt compile*

**Test**

Utilisez la commande suivante pour lancer les tests unitaires

*sbt test*

**Run**

Utilisez la commande suivante pour executer le programme 
 
*sbt "run fileName"* 

où fileName est le nom du fichier fournit en entrée


## Description des class
Dans cette partie figure la description et la justification de certain choix d'implémentation.

**Show**

Show est typeclass qui fournit une methode permettant de print un objet qui possederait un
implicit Show[T] ou T est son type

**Mapp**

Mapp represente la carte du programme. C'est dessus qu'une tondeuse va pouvoir s'y déplacer.
Une map ne possède pas de tondeuse mais ce sont les tondeuses qui sont associés à une map.

Pour instancier une mapp depuis une chaine de caractères, il faut appeller  la méthode
fromString de l'objet  compagnon Mapp.
Cette dernière renvoie un Option[Mapp] en fonction de la chaine de caractère (si elle est correcte ou non)


**Megatondeuse**

Le main du programme. Il va s'occuper du parsing du fichier
J'ai également pensé à implémenter un typeclass Readeable et leguer la lecture des lignes (par exemple à un TondeuseRead), mais je n'ai pas trouvé de solution fonctionnel.

**Orientation**

Les orientations possible d'une tondeuse (N E W S)

**Tondeuse**

Représente une tondeuse, elle peut, a l'aide d'un objet compagnon, tourner de 90° à gauche/droite, avancer depuis sa position actuelle
et executer une suite d'instructions sous la forme d'une List[Char].
Bien que j'aurais pu utiliser une String directement, j'ai préféré utilisé une List pour pouvoir faire QUE du patterm matching 

De la même manière qu'une Mapp, pour instancier une Tondeuse depuis une chaîne de caractère, il faut appeller la méthode fromString de l'objet compagnon Tondeuse 

Par ailleurs pour gerer le cas où la liste d'instruction est corrompu, j'utilise la structure Try[T] (avec Success et Failure) qui
est plus cohérent qu'un Option[T] (bien que ca aurait marché avec ce dernier). 