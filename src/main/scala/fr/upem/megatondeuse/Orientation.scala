package fr.upem.megatondeuse

sealed trait Orientation {val value : String}

case object North extends Orientation { val value = "N"}
case object South extends Orientation { val value = "S"}
case object West extends Orientation {val value = "W"}
case object East extends Orientation { val value = "E"}