package fr.upem.megatondeuse


trait Show[T] {
  def show(t: T) : String
}

object Show{

  def print[T](t: T)(implicit ev: Show[T]) : String =  ev.show(t)
}

