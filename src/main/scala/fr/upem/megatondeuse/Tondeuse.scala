package fr.upem.megatondeuse
import scala.util.matching.Regex
import scala.util.{Failure, Success, Try}
case class Tondeuse private(x : Int, y : Int, orientation : Orientation, map : Mapp)


object Tondeuse {

  lazy val tondeusePattern: Regex = "([0-9]+) ([0-9]+) ([A-Za-z])".r

  implicit val show: Show[Tondeuse] = new Show[Tondeuse] {
    override def show(t: Tondeuse): String = s"${t.x} ${t.y} ${t.orientation.value}"
  }


  def apply(x: Int, y: Int, orientation: Orientation, map: Mapp): Option[Tondeuse] =
    if (x >= 0 && x <= map.sizeX && y >= 0 && y <= map.sizeY)
      Some(new Tondeuse(x, y, orientation, map))
    else
      None

  /**
    * Instantiate a Tondeuse from a String with form "X Y O" with X and Y >= 0 and O in { N, E, W, S}
    * @param state
    * @param map
    * @return
    */
  def fromString(state: String, map: Mapp): Option[Tondeuse] ={
    state match {
      case tondeusePattern(x, y, orientation) =>
        orientation match {
          case "N" => Tondeuse(x.toInt, y.toInt, North, map)
          case "S" => Tondeuse(x.toInt, y.toInt, South, map)
          case "E" => Tondeuse(x.toInt, y.toInt, East, map)
          case "W" => Tondeuse(x.toInt, y.toInt, West, map)
          case _ => None
        }
      case _ => None
    }
  }
  /**
    * Do a 90° turn to the left
    */
  def turnLeft(t: Tondeuse) : Tondeuse = {
    t.orientation match {
      case North => Tondeuse(t.x, t.y, West, t.map).get
      case West => Tondeuse(t.x, t.y, South, t.map).get
      case South => Tondeuse(t.x, t.y, East, t.map).get
      case East => Tondeuse(t.x, t.y, North, t.map).get
    }
  }

  /**
    * Do a 90° turn to the right
    */
  def turnRight(t: Tondeuse) : Tondeuse ={
    t.orientation match {
      case North => Tondeuse(t.x, t.y, East, t.map).get
      case West => Tondeuse(t.x, t.y, North, t.map).get
      case South => Tondeuse(t.x, t.y, West, t.map).get
      case East => Tondeuse(t.x, t.y, South, t.map).get
    }
  }

  /**
    * Move Tondeuse foward on map.
    * The Tondeuse will be blocked it it tries to move out from the map
    *
    */
  def move(t: Tondeuse) : Tondeuse = {
    t.orientation match {

      case South => if (t.y <= 0) Tondeuse(t.x, t.y, t.orientation, t.map).get else Tondeuse(t.x, t.y - 1, t.orientation, t.map).get
      case West => if (t.x <= 0) Tondeuse(t.x, t.y, t.orientation, t.map).get else Tondeuse(t.x - 1, t.y, t.orientation, t.map).get
      case North => if (t.y >= t.map.sizeY) Tondeuse(t.x, t.y, t.orientation, t.map).get else Tondeuse(t.x, t.y + 1, t.orientation, t.map).get
      case East => if (t.x >= t.map.sizeX) Tondeuse(t.x, t.y, t.orientation, t.map).get else Tondeuse(t.x + 1, t.y, t.orientation, t.map).get
    }
  }

  /**
    * Run the tondeuse.
    * At the end of the instructions, the function print the current position and orientation
    * @param instructions : String
    * @return Try[Tondeuse]
    */
  def run(t: Tondeuse, instructions : List[Char]) : Try[Tondeuse] = {

    instructions match {

      case Nil => Failure(new IllegalArgumentException)
      // Derniere instruction

      case instruction :: Nil =>
        instruction match {
          case 'A' =>
            val tondeuse = move(t)
            println(Show.print(tondeuse))
            Success(tondeuse)
          case 'G' =>
            val tondeuse = turnLeft(t)
            println(Show.print(tondeuse))
            Success(tondeuse)
          case 'D' =>
            val tondeuse = turnRight(t)
            println(Show.print(tondeuse))
            Success(tondeuse)
          case _ => Failure(new IllegalArgumentException)
        }
      case  instruction :: rest =>

        instruction match {
          case 'A' => run(move(t), rest)
          case 'G' => run(turnLeft(t), rest)
          case 'D' => run(turnRight(t), rest)
          case _ => Failure(new IllegalArgumentException)
        }
    }
  }



}


