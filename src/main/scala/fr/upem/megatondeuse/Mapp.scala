package fr.upem.megatondeuse

import scala.util.matching.Regex

case class Mapp(sizeX : Int, sizeY : Int)

object Mapp{


  lazy val mappPattern: Regex = "([0-9]+) ([0-9]+)".r

  def apply(sizeX: Int,  sizeY: Int): Option[Mapp] =
    if (sizeX > 0 && sizeY > 0)
      Some(new Mapp(sizeX, sizeY))
    else
      None


  /**
    * Create a instance of Mapp from String. The format of the string must be "X Y" with X and Y > 0
    * @param state
    * @return
    */
  def fromString(state: String) : Option[Mapp] = {
    state match {
      case mappPattern(sizeX, sizeY) => Mapp(sizeX.toInt, sizeY.toInt)
      case _ => None
    }

  }

  implicit val show = new Show[Mapp] {
    override def show(t: Mapp): String = s"Map size is ${t.sizeX} ${t.sizeY}"
  }
}
