package fr.upem.megatondeuse

import scala.io.Source
import scala.util.{Failure, Success, Try}

object MegaTondeuse extends App {


  def usage() : Unit = {
    println(
      """Le programme necessite un argument qui correspond au fichier nécessaire au lancement du programme.
        | La première ligne de ce fichier correspond à la taille (X Y) de la taille.
        | Les lignes suivantes correspondent aux tondeuses (deux lignes de chaque).
        | La première ligne pour l'initilisation d'une tondeuse dont le format est le suivant : X Y O où X et Y
        |  sont les positions initial de la tondeuse et O l'orientation qui est une valeur dans {N,E,W,S}.
        |  La seconde ligne pour les instructions de la tondeuse. Il s'agit d'une suite de caractères compris dans
        |  { A (Avancer), G (Gauche), D (Droite) }
        |
        |  Un exemple de fichier est présent dans src/main/resource
      """.stripMargin)
  }


  def process(tail: List[String], mapp : Mapp): Try[Tondeuse] = {
    tail match {

      case Nil =>
        println("Le fichier présente un problème de format. Le programme va quitter.")
        Failure(new IllegalArgumentException)

      // CASE : Seulement une ligne => Probleme de format
      case _ :: Nil =>
        println("Le fichier présente un problème de format : Une tondeuse n'a pas d'instruction. Le programme va quitter")
        Failure(new IllegalArgumentException)

      //CASE : Deux lignes
      case l1 :: instructions :: Nil =>
        val tondeuse = Tondeuse.fromString(l1, mapp)

        tondeuse match {
          case Some(t) =>
            Tondeuse.run(t, instructions.toList) match {
              case Success(t1) =>
                println("Le programme s'est correctement executé")
                Success(t1)
              case Failure(e) =>
                println("Le fichier présente un problème de format : " + instructions + ". Le programme va quitter")
                Failure(e)
            }
          case None =>
            println("Le fichier présente un problème de format : " + l1 + ". Le programme va quitter")
            Failure(new IllegalArgumentException)
        }

      //CASE : Au moins 3 lignes
      case l1 :: instructions :: rest =>

        val tondeuse = Tondeuse.fromString(l1, mapp)

        tondeuse match {

          case Some(t) =>

            Tondeuse.run(t, instructions.toList) match {
              case Success(_) => process(rest, mapp)
              case Failure(e) =>
                println("Le fichier présente un problème de format : " + instructions + ". Le programme va quitter")
                Failure(e)

            }

          case None =>
            println("Le fichier présente un problème de format : " + l1 + ". Le programme va quitter")
            Failure(new IllegalArgumentException)


        }
    }
  }

  override def main(args: Array[String]): Unit = {
    if (args.isEmpty){
      usage()
      System.exit(0)
    }
    val lines = Source.fromFile(args(0)).getLines().toList

    lines match {
      case head :: tail =>

        val mapp = Mapp.fromString(head)

        mapp match {
          case Some(m) => process(tail, m)
          case _ =>
            println("Un problème est survenu lors du parsing de la ligne 1. Le programme va quitter")
            System.exit(0)
        }

      case _ =>
        usage()
        System.exit(0)
    }



  }
}
