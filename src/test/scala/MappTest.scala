import org.scalatest.{FlatSpec, Matchers}
import fr.upem.megatondeuse._
import fr.upem.megatondeuse.Mapp.show

class MappTest extends FlatSpec with Matchers  {

  "Bad mapp instantiation" should "be None" in {
    Mapp.fromString("-1 0") should be(None)
  }

  "Bad mapp instantiation from bad format string" should "be None" in {
    Mapp.fromString("0 0 23") should be(None)
  }

  "Bad mapp instantiation from bad format string 2" should "be None" in {
    Mapp.fromString("SDZ 0 ") should be(None)
  }

  " mapp instantiation 90 15" should "Map size is 90 15" in {
    Show.print(Mapp.fromString("90 15").get) should be("Map size is 90 15")
  }

}
