import org.scalatest.{FlatSpec, Matchers}
import fr.upem.megatondeuse._
import fr.upem.megatondeuse.Tondeuse.show


class TondeuseTest extends FlatSpec with Matchers {

  "Bad tondeuse instantiation" should "be None" in {
    Tondeuse.fromString("-1 0 Z", Mapp(10, 10).get) should be(None)
  }

  "Bad tondeuse instantiation from bad format instructions" should "be None" in {
    Tondeuse.fromString("0 0 N Dsdzadsqdz", Mapp(10, 10).get) should be(None)
  }

  "Bad tondeuse instantiation from bad format instructions 2" should "be None" in {
    Tondeuse.fromString("0 A N ", Mapp(10, 10).get) should be(None)
  }

  "Instantiation from string 0 0 N" should "0 0 N" in {
    Show.print(Tondeuse.fromString("0 0 N", Mapp(10, 10).get).get) should be("0 0 N")
  }


  "Instantiation from string 42 42 N" should "42 42 N" in {
    Show.print(Tondeuse.fromString("42 42 N", Mapp(50, 50).get).get) should be("42 42 N")
  }


  "Instantiation from string 11 11 N on Map(10, 10)" should "be None" in {
    Tondeuse.fromString("11 11 N", Mapp(10, 10).get) should be(None)
  }

  "A Tondeuse move from 0 0 E" should "be 1 0 E" in {
    val tondeuse = Tondeuse(0, 0, East, Mapp(10, 10).get).get

    Show.print(Tondeuse.run(tondeuse, "A".toList).get) should be("1 0 E")

  }

  "A Tondeuse move from 0 0 N" should "be 0 1 N" in {
    val tondeuse = Tondeuse(0, 0, North, Mapp(10, 10).get).get

    Show.print(Tondeuse.run(tondeuse, "A".toList).get) should be("0 1 N")

  }

  "A Tondeuse move from 9 9 N" should "be 9 10 N" in {
    val tondeuse = Tondeuse(9, 9, North, Mapp(10, 10).get).get

    Show.print(Tondeuse.run(tondeuse, "A".toList).get) should be("9 10 N")

  }

  "A Tondeuse move from 0 0 W" should "0 0 W" in {
    val tondeuse = Tondeuse(0, 0, West, Mapp(10, 10).get).get

    Show.print(Tondeuse.run(tondeuse, "A".toList).get) should be("0 0 W")

  }

  "A Tondeuse move from 1 0 W" should "0 0 W" in {
    val tondeuse = Tondeuse(1, 0, West, Mapp(10, 10).get).get

    Show.print(Tondeuse.run(tondeuse, "A".toList).get) should be("0 0 W")

  }


  "A Tondeuse move from 1 1 W with instructions AGA" should "0 0 S" in {
    val tondeuse = Tondeuse(1, 1, West, Mapp(10, 10).get).get

    Show.print(Tondeuse.run(tondeuse, "AGA".toList).get) should be("0 0 S")

  }

  "A Tondeuse move from 1 1 W with instructions AGAD" should "0 0 N" in {
    val tondeuse = Tondeuse(1, 1, West, Mapp(10, 10).get).get

    Show.print(Tondeuse.run(tondeuse, "AGAD".toList).get) should be("0 0 W")

  }

}
